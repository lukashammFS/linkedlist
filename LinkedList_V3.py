# -*- coding: utf-8 -*-
"""
Created on Mon Nov  2 13:19:47 2020

@author: lukas
"""

## Linked List

#creation of class
class Node:
    def __init__(self, data, link = None):
        self.data = data
        self.link = link
        
    def __repr__(self):
        return self.data, self.link

    def insert(data, after_element):
        new_node = Node(data)
        current = linkedlist.head
        while current != after_element and current.link != None:
            current = current.link
        if current == after_element:
            new_node.link = current.link
            current.link = new_node
            return
        return "Element not found in list"
        
            

#creation of initial list
node1 = Node(1)
node2 = Node('some_str', node1)
linkedlist = Node(12, node2)
linkedlist.head = linkedlist

#insertion of elements
Node.insert(5, linkedlist)

#measuring time complexity
import time
import pandas as pd
import matplotlib.pyplot as plt

iterations_array = [10, 100000, 500000, 1000000, 2000000, 3000000, 5000000]
times = []

for elements in iterations_array:
    test_linkedlist = linkedlist
    test_list = list(range(1, elements+1))
    time_start = time.time()
    for i in test_list:
        Node.insert(1, test_linkedlist)
    time_end = time.time()
    times.append(time_end - time_start)
        
results = pd.DataFrame({'Iterations': iterations_array,
                       'Time': times})

plt.plot(results.Time, results.Iterations)
plt.xlabel('n') 
plt.ylabel('seconds') 
        
        